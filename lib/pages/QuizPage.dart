import 'package:flutter/material.dart';
import 'package:kvizapp/model/question.dart';
import 'package:kvizapp/pages/QuestionOptions.dart';
import 'package:kvizapp/pages/QuizResult.dart';
import 'package:http/http.dart' as http;

import 'dart:async';
import 'dart:convert';

class StartGame extends StatefulWidget {
  final String playerName;
  final String selectedCategory;
  const StartGame(
      {Key? key, required this.playerName, required this.selectedCategory})
      : super(key: key);

  @override
  State<StartGame> createState() => _StartGameState();
}

class _StartGameState extends State<StartGame> {
  List<Question> questions = [];
  int currentQuestionIndex = 0;
  int correctAnswersCount = 0;
  List<String> rotatedAnswers = [];
  bool isLoading = true;
  late Timer _timer;
  int _timeRemaining = 20;
  bool answeredQuestion = false;

  Future<void> api() async {
    final response = await http.get(Uri.parse(widget.selectedCategory));
    if (response.statusCode == 200) {
      var responseData = response.body;
      var decodedResponse = utf8.decode(response.bodyBytes);
      var data = jsonDecode(decodedResponse)['results'] as List<dynamic>;
      setState(() {
        questions = data
            .map((question) => Question(
                  type: question['type'],
                  difficulty: question['difficulty'],
                  category: question['category'],
                  question: question['question'],
                  correctAnswer: question['correct_answer'],
                  incorrectAnswers:
                      List<String>.from(question['incorrect_answers']),
                ))
            .toList();
        isLoading = false;
        shuffeldOpstions(); // Megkeverjük a válaszokat a kérdések újratöltésekor
      });
    } else {
      AlertDialog(
          semanticLabel: 'Hiba történt a kérés során: ${response.statusCode}');
    }
  }

  @override
  void initState() {
    super.initState();
    api();
    startTimer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: AppBar(
          title: Text(
            "${widget.playerName}",
            style: const TextStyle(
                fontSize: 19, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          backgroundColor: Colors.deepPurple,
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/7290759.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: isLoading
            ? const Center(child: CircularProgressIndicator())
            : Padding(
                padding: const EdgeInsets.all(18),
                child: Column(
                  children: [
                    Container(
                      height: 80,
                      width: 380,
                      decoration: BoxDecoration(
                          color: const Color(0xffA42FC1),
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(Icons.punch_clock_rounded),
                          Text(
                            "Óra: ${_timeRemaining.toString()}",
                            style: const TextStyle(
                                fontSize: 15, color: Colors.white),
                          ),
                          const SizedBox(
                            width: 80,
                          ),
                          Text("Pontszám: ${correctAnswersCount.toString()}",
                              style: const TextStyle(
                                  fontSize: 15, color: Colors.white))
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SizedBox(
                        height: 550,
                        width: 480,
                        child: Center(
                          child: Flexible(
                            child: Container(
                              constraints: const BoxConstraints(
                                minHeight: 550.0,
                              ),
                              width: 390,
                              decoration: BoxDecoration(
                                  color: const Color(0xffA42FC1),
                                  borderRadius: BorderRadius.circular(20)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  verticalDirection: VerticalDirection.down,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(
                                      " Kérdés: ${currentQuestionIndex + 1}/${questions.length}",
                                      style: const TextStyle(
                                          color: Colors.white, fontSize: 20),
                                    ),
                                    Text(
                                      questions.isNotEmpty
                                          ? questions[currentQuestionIndex]
                                              .question
                                          : '',
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 24,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    const SizedBox(
                                      height: 50,
                                    ),
                                    Flexible(
                                      child: Column(
                                        children: (questions.isNotEmpty &&
                                                questions[currentQuestionIndex]
                                                        .incorrectAnswers !=
                                                    null)
                                            ? rotatedAnswers.map((option) {
                                                // Meghatározzuk, hogy a válasz helyes-e vagy sem
                                                bool isCorrect = option ==
                                                    questions[
                                                            currentQuestionIndex]
                                                        .correctAnswer;
                                                // Minden válasz lehetőséget továbbítunk, beleértve, hogy helyes-e vagy sem
                                                return Answers(
                                                  option: option.toString(),
                                                  isCorrect: isCorrect,
                                                  onAnswerSelected:
                                                      onAnswerSelected,
                                                  answered: answeredQuestion,
                                                );
                                              }).toList()
                                            : [],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 50,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 18),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: const Color(0xffA42FC1),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 40, vertical: 25),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18))),
                        onPressed: () {
                          setState(() {
                            answeredQuestion = false;
                            if (currentQuestionIndex < questions.length - 1) {
                              currentQuestionIndex++;
                              shuffeldOpstions(); // Megkeverjük a válaszokat az új kérdés megjelenítésekor
                              _timeRemaining = 20; // Visszaállítjuk az időzítőt
                              _timer
                                  .cancel(); // Az aktuális időzítőt leállítjuk
                              startTimer(); // Új időzítőt indítunk a következő kérdéshez
                            } else {
                              // Ha elérjük a 10 kérdést, akkor irányítsunk át a QuizResult oldalra
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Result(
                                    playerName: widget.playerName,
                                    correctAnswersCount: correctAnswersCount,
                                  ),
                                ),
                              );
                            }
                          });
                        },
                        child: const Text(
                          "Következő kérdés",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  void onAnswerSelected(bool isCorrect) {
    setState(() {
      if (isCorrect) {
        correctAnswersCount++;
      }
      answeredQuestion = true;
    });
  }

  void shuffeldOpstions() {
    rotatedAnswers = rotatedAnswer([
      questions[currentQuestionIndex].correctAnswer,
      ...(questions[currentQuestionIndex].incorrectAnswers as List)
    ]);
  }

  List<String> rotatedAnswer(List<String> option) {
    List<String> shuffeldOpstions = List.from(option);
    shuffeldOpstions.shuffle();
    return shuffeldOpstions;
  }

  void startTimer() {
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        if (_timeRemaining > 0) {
          _timeRemaining--;
        } else {
          // Ha lejár az idő, ellenőrizzük, hogy van-e még további kérdés
          if (currentQuestionIndex < questions.length - 1) {
            currentQuestionIndex++; // Ugrás a következő kérdésre
            shuffeldOpstions(); // Megkeverjük a válaszokat az új kérdés megjelenítésekor
            _timeRemaining = 20; // Visszaállítjuk az időzítőt
          } else {
            // Ha már nincs több kérdés, irányítsunk át a QuizResult oldalra
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Result(
                  playerName: widget.playerName,
                  correctAnswersCount: correctAnswersCount,
                ),
              ),
            );
            _timer.cancel(); // Leállítjuk az időzítőt
          }
        }
      });
    });
  }
}
