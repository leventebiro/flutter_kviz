import 'package:flutter/material.dart';
import 'package:kvizapp/pages/QuizPage.dart';

class QuizzStartPage extends StatefulWidget {
  const QuizzStartPage({super.key});
  @override
  State<QuizzStartPage> createState() => _QuizzStartPageState();
}

class _QuizzStartPageState extends State<QuizzStartPage> {
  String _playerName = '';
  String selectedCategory = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/7290759.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Container(
            width: 350,
            height: 350,
            decoration: BoxDecoration(
              color: const Color(0xffA42FC1),
              border: Border.all(color: Colors.black),
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Image(
                  image: AssetImage("assets/quizzlogo.png"),
                  width: 80,
                  height: 80,
                ),
                const Spacer(),
                Text(
                  "Játékos név",
                  style: Theme.of(context).textTheme.headlineLarge?.copyWith(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  margin:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: TextField(
                    decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: "kérek egy nevet ide..."),
                    onChanged: (text) {
                      setState(() {
                        _playerName = text;
                      });
                    },
                  ),
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    const Text("Általános"),
                    Radio(
                      value: 1,
                      groupValue: selectedCategory,
                      onChanged: (value) {
                        setState(() {
                          selectedCategory =
                              "https://opentdb.com/api.php?amount=10&category=9&type=multiple";
                        });
                      },
                    ),
                    const Text("Film"),
                    Radio(
                      value: 2,
                      groupValue: selectedCategory,
                      onChanged: (value) {
                        setState(() {
                          selectedCategory =
                              "https://opentdb.com/api.php?amount=10&category=11&type=multiple";
                        });
                      },
                    ),
                    const Text("Zene"),
                    Radio(
                      value: 3,
                      groupValue: selectedCategory,
                      onChanged: (value) {
                        setState(() {
                          selectedCategory =
                              "https://opentdb.com/api.php?amount=10&category=12&type=multiple";
                        });
                      },
                    ),
                    const Text("Tudomány"),
                    Radio(
                      value: 4,
                      groupValue: selectedCategory,
                      onChanged: (value) {
                        setState(() {
                          selectedCategory =
                              "https://opentdb.com/api.php?amount=10&category=17&type=multiple";
                        });
                      },
                    ),
                  ],
                ),
                const SizedBox(
                    height: 20), // Space between container and button
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => StartGame(
                            playerName: _playerName,
                            selectedCategory: selectedCategory),
                      ),
                    );
                  },
                  child: const Text("Játék indul"),
                ),
                const SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
