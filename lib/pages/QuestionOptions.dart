import 'package:flutter/material.dart';

class Answers extends StatelessWidget {
  final String option;
  final bool isCorrect;
  final Function(bool) onAnswerSelected;
  final bool answered;

  const Answers({
    required this.option,
    required this.isCorrect,
    required this.onAnswerSelected,
    required this.answered,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 55,
          width: 250,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(width: 3, color: Colors.white60)),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: ElevatedButton(
                      onPressed: answered
                          ? null
                          : () {
                              onAnswerSelected(isCorrect);
                            },
                      child: Text(option),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
