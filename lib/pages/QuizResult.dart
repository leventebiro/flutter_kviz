import 'package:flutter/material.dart';
import 'package:kvizapp/pages/MyHomePage.dart';

class Result extends StatelessWidget {
  final String playerName;
  final int correctAnswersCount;

  const Result({
    Key? key,
    required this.playerName,
    required this.correctAnswersCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/7290759.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Container(
            width: 350,
            height: 250,
            decoration: BoxDecoration(
              color: const Color(0xffA42FC1),
              border: Border.all(color: Colors.black),
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Gratulálok, $playerName!',
                  style: const TextStyle(fontSize: 24),
                ),
                const SizedBox(height: 20),
                Text(
                  'A helyes válaszok száma: $correctAnswersCount.',
                  style: const TextStyle(fontSize: 18),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const QuizzStartPage(),
                      ),
                    );
                  },
                  child: const Text("Új játék"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
